module.exports = function(grunt) {
    grunt.initConfig({
        //tasks in here in object notation
        less: {
            dev: {
                options: {
                    compress: false,
                    sourceMap: true,
                    dumpLineNumbers: 'comments',
                    plugins: [
                        new(require('less-plugin-autoprefix'))({ browsers: ["last 2 versions"] })
                    ],
                },
                files: {
                    "css/snaap_custom.css": "css/less/snaap_custom.less",
                }
            },
            dist: {
                options: {
                    compress: true,
                    sourceMap: false,
                    plugins: [
                        new(require('less-plugin-autoprefix'))({ browsers: ["last 2 versions"] })
                    ],
                },
                files: {
                    "css/snaap_custom.css": "css/less/snaap_custom.less",
                }
            },
        },
        jshint: {
            all: ['js/**/*.js']
        },
        processhtml: {
            dist: {
                files: {
                    'index.html': ['index-dev.html']
                }
            }
        },
        watch: {
            html: {
                files: ['*.html'],
                options: {
                    livereload: true,
                }
            },
            css: {
                files: ['css/less/**/*.less'],
                tasks: ['less:dev'],
                options: {
                    livereload: true,
                }
            },
            scripts: {
                files: ['javascript/snaap_custom.js'],
                tasks: ['jshint'],
                options: {
                    event: ['all'],
                    // livereload: true,
                }
            }
        },
        copy: {
            js: {
                expand: true,
                src: "javascript/*",
                dest: 'dist',
            },
            css: {
                expand: true,
                src: "css/*",
                dest: 'dist',
            },
            html: {
                expand: true,
                src: "index.html",
                dest: 'dist',
            },
        },
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['less:dev']);
    grunt.registerTask('live', ['less:dist', 'processhtml', 'copy']);
};
