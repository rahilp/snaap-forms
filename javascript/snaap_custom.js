$(document).ready(function($) {
    sid = $('.snaap-select select').attr('id');
    $('.snaap-select').append('</ul>').prepend('<ul id="' + sid + '-option">');
    $('.snaap-select select option').each(
        function() {
            $('.snaap-select ul').append('<li class="' + $(this).val() + '">' + $(this).text() + '</li>');
        }
    );
    var o1 = $('.snaap-select ul li:first').text();
    $('.snaap-select').prepend('<div class="snaap-select--field"></div>');
    $('.snaap-select--field').text(o1);

    // Show hide the Options
    $('.snaap-select--field').click(function() {
        $('.snaap-select ul').toggleClass('snaap-select--field---active');
    });

    // Click Change selected item
    $('.snaap-select ul li').click(function(event) {
        $('.snaap-select ul').removeClass('snaap-select--field---active');
        var li_text = $(this).text();
        var opv = $(this).attr('class');
        $('.snaap-select--field').text(li_text);
        $(".snaap-select select").val(opv);
    });

    $(document).mouseup(function(e) {
        var container = $(".snaap-select--field---active");

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            container.removeClass('snaap-select--field---active');
        }
    });
});
